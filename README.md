# WLAN Monitor zur Nacht des Wissens 2018

Was ist das für ein Gefühl, wenn auf einem riesigen Monitor die Namen der WLANs 
des eigenen Zuhause, der Firma oder des letzten Urlaubes auftauchen und es so 
aussieht als würde eine Verbindung mit genau diesen Netzwerken hergestellt werden?

https://hacklabor.de/2018/10/wlanmonitor/

![](https://hacklabor.de/assets/blog/wlanmonitor.jpg)

## Aufbau

Das System besteht aus einem Raspi 3 mit einem [Wifi-Adapter](https://amzn.to/2NWZC2c), der in den Monitor-Modus
versetzt werden kann. Die Ausgabe erfolgt über einen Monitor.

Auf dem Raspi läuft eine Desktop Raspbian.

## Einrichtung

  * Repo clonen
  * in das Verzeichnis wechseln

```
pip install netaddr
pip install scapy
sudo apt install nodejs npm aircrack-ng
npm install
```

## Monitor starten

Das WLAN Interface in den Monitor-Mode setzen:

```
airmon-ng start wlan1
```

Skript starten:

```
sudo python scripts/probemon.py -i wlan1mon -t unix -s -l | node server.js
```

Für die Ausgabe im Browser:

```
http://localhost:3000
```

## Monitor beenden

Skript abbrechen und den Monitormode beenden:

```
airmon wlan1mon stop
```

## Credits

  * https://github.com/nikharris0/probemon

## Links

  * [WLAN-Hacking: Monitor Mode](https://www.elektronik-kompendium.de/sites/net/2008051.htm)
  * [Buy the Best Wireless Network Adapter for Wi-Fi Hacking in 2018](https://null-byte.wonderhowto.com/how-to/buy-best-wireless-network-adapter-for-wi-fi-hacking-2018-0178550/)
  * [Scapy - Packet crafting for Python2 and Python3](https://scapy.net/)
  * 
  