var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res) {
   res.sendfile('index.html');
});

var clients = 0;
io.on('connection', function(socket) {
   clients++;
   socket.emit('newclientconnect',{ description: 'Hey, welcome!'});
   socket.broadcast.emit('newclientconnect',{ description: clients + ' clients connected!'})
   socket.on('disconnect', function () {
      clients--;
      socket.broadcast.emit('newclientconnect',{ description: clients + ' clients connected!'})
   });
});

http.listen(3000, function() {
   console.log('listening on localhost:3000');
});

process.stdin.setEncoding('utf8');
process.stdin.on('readable', function() {
  const chunk = process.stdin.read();
  if (chunk !== null) {
    var re = /\S*\s\S*\s(\S*)/;
    var ssid = chunk.replace(re, "$1").trim(); 
    if (ssid.startsWith('HITRON')) ssid = '';
    if (ssid.startsWith('Vodafone')) ssid = '';
    if (ssid.startsWith('KabelBox')) ssid = '';
    if (ssid.startsWith('EasyBox')) ssid = '';
    if (ssid.startsWith('Telekom_FON')) ssid = '';
    if (ssid.startsWith('o2-WLAN')) ssid = '';
    if (ssid.startsWith('FRITZ!Box')) ssid = '';
    if (ssid.startsWith('KDG')) ssid = '';
    if (ssid.startsWith('Hacklabor')) ssid = '';
    console.log(ssid);
    if (ssid) {
      io.emit('ssid', ssid);
    }
  } 
})

